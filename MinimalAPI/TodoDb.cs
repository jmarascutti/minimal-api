﻿using Microsoft.EntityFrameworkCore;
using MinimalAPI.Models;

namespace MinimalAPI
{
    public class TodoDb : DbContext
    {
        public TodoDb(DbContextOptions<TodoDb> options)
            : base(options) { }

        //contexto
        public DbSet<Todo> Todos => Set<Todo>();
         }
}
